Vue component to overlay Figma layouts and components during front end development. Inspired by [Perfect Pixel](https://www.welldonecode.com/perfectpixel/) and unleashing [Figma API](https://www.figma.com/developers/api).

[All components](https://vdcrea.gitlab.io/vue-perfect-figma/)

[Gitlab repository](https://gitlab.com/vdcrea/vue-perfect-figma)

Install
```html
npm i -S gitlab:vdcrea/vue-perfect-figma
```

In your `main.js`, use globally only during development:
```html
import Vue from 'vue'
if (process.env.NODE_ENV === 'development') {
  const VuePerfectFigma = require('vue-perfect-figma')
  Vue.use(VuePerfectFigma)
}
```

Ask your designer to provide you with a [Figma access Token](https://www.figma.com/developers/api#access-tokens)

Ask (again) your designer to provide you the id of the `file` and the `node-ids` you wish to develop. He can see it easily in the url:
```html
https://www.figma.com/file/{file-id}/document-title?node-id={node-id}
```

Then in your template you can use `<PerfectFigma />` components, wrapping the component your are working on:
```html
<PerfectFigma
  token="figma-access-token"
  file="figma-file-id"
  nodes="figma-node-ids-separated-by-coma">

  <div class="my-awesome-component">
    ...
  </div>

</PerfectFigma>
```

Remember to remove all `<PerfectFigma />` components from your templates, before building your application.
