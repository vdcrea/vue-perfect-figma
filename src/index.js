import PerfectFigma from './components/PerfectFigma'
import PerfectFigmaBox from './components/PerfectFigmaBox'
import PerfectFigmaButton from './components/PerfectFigmaButton'
import PerfectFigmaContainer from './components/PerfectFigmaContainer'
import PerfectFigmaFolder from './components/PerfectFigmaFolder'
import PerfectFigmaImage from './components/PerfectFigmaImage'
import PerfectFigmaOverlay from './components/PerfectFigmaOverlay'
import PerfectFigmaPath from './components/PerfectFigmaPath'
import PerfectFigmaSettings from './components/PerfectFigmaSettings'
import PerfectFigmaSettingsDisplay from './components/PerfectFigmaSettingsDisplay'
import PerfectFigmaSettingsOpacity from './components/PerfectFigmaSettingsOpacity'
import PerfectFigmaSpinner from './components/PerfectFigmaSpinner'

export function install (Vue) {
  Vue.component('PerfectFigma', PerfectFigma)
}

export {
  PerfectFigma,
  PerfectFigmaBox,
  PerfectFigmaButton,
  PerfectFigmaContainer,
  PerfectFigmaFolder,
  PerfectFigmaImage,
  PerfectFigmaOverlay,
  PerfectFigmaPath,
  PerfectFigmaSettings,
  PerfectFigmaSettingsDisplay,
  PerfectFigmaSettingsOpacity,
  PerfectFigmaSpinner
}

const plugin = {
  version: process.env.VUE_APP_VERSION,
  install
}

export default plugin

let GlobalVue = null
if (typeof window !== 'undefined') {
  GlobalVue = window.Vue
} else if (typeof global !== 'undefined') {
  GlobalVue = global.Vue
}
if (GlobalVue) {
  GlobalVue.use(plugin)
}
