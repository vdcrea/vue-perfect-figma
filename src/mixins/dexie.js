
import Dexie from 'dexie'

export default {
  created () {
    const stores = {
      figma: 'url, ts',
      graphics: 'id, ts'
    }
    if (!window.pfdx) {
      window.pfdx = new Dexie('[PF]')
      window.pfdx.version(1).stores(stores)
    }
    const ts = Date.now()
    const offset = this.cache * 60 * 60 * 1000
    const tables = Object.keys(stores)
    for (const t of tables) {
      window.pfdx[t].where('ts')
        .below(ts - offset)
        .toArray(docs => {
          const keys = docs.map(doc => doc.id)
          window.pfdx[t].bulkDelete(keys)
        })
    }
  }
}
