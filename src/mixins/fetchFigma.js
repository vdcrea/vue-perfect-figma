import PerfectFigmaSpinner from '@/components/PerfectFigmaSpinner'

export default {
  components: {
    PerfectFigmaSpinner
  },
  props: {
    api: {
      type: String,
      default: 'https://api.figma.com/v1/'
    },
    /**
     * Figma access token
     * https://www.figma.com/developers/api#access-tokens
     */
    token: {
      type: String,
      required: true
    },
    /**
     * Figma file id
     * Visible in the url, it s the unique id right after /file/
     */
    file: {
      type: String,
      required: true
    }
  },
  data () {
    return {
      loading: false
    }
  },
  methods: {
    figmaApi (url) {
      return fetch(url, { headers: { 'X-Figma-Token': this.token } })
    },
    endpointNodes (ids) {
      return this.api + 'files/' + this.file + '/nodes?ids=' + ids
    },
    endpointGraphics (ids) {
      return this.api + 'images/' + this.file + '?ids=' + ids + '&format=jpg'
    },
    fetchFigma (ids, img) {
      return new Promise((resolve, reject) => {
        this.loading = true
        let url = this.endpointNodes(ids)
        if (img) url = this.endpointGraphics(ids)
        window.pfdx.figma.get(url).then(res => {
          if (res) {
            this.loading = false
            resolve(res.json)
          } else {
            this.figmaApi(url)
              .then(res => res.json())
              .then(json => {
                const ts = Date.now()
                window.pfdx.figma.put({ url, ts, json })
                resolve(json)
              })
              .catch(err => reject(err))
              .finally(() => {
                this.loading = false
              })
          }
        })
      })
    }
  }
}
