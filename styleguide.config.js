const path = require('path')

module.exports = {
  title: 'Vue Perfect Figma',
  components: 'src/components/**/[A-Z]*.vue',
  usageMode: 'expand',
  exampleMode: 'expand',
  styleguideDir: 'public',
	getComponentPathLine(componentPath) {
    const component = path.basename(componentPath, '.vue').split('.')[0]
    const dir = path.dirname(componentPath)
    return `import { ${component} } from 'vue-perfect-figma'`
  },
  sections: [
    {
      name: 'Vue Perfect Figma',
      content: 'readme.md'
    },
    {
      name: 'Components',
      components: 'src/components/**/*.vue'
    }
  ]
}
